package com.example.test;



import org.apache.cordova.DroidGap;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;


/**
 * Launches an external application.
 *
 * @author Emmmanouel <androidkrayze@gmail.com>
 * @license MIT/X11
 */
public class StartApp extends CordovaPlugin
{
	
	String TAG = "Viessmann";
    /**
     * Executes the request and returns PluginResult.
     *
     * @param action
     *          Action to perform.
     * @param args
     *          Arguments to the action.
     * @param callbackId
     *          JavaScript callback ID.
     * @return A PluginResult object with a status and message.
     */
	@Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext)throws JSONException
    {
		if(action.equals("startApp")){
			//Log.i(TAG, "CLASS CALLED");
			 String component = args.getString(0);
	            this.startActivity(component, callbackContext);
	            return true;
			
		}
		return false;

    }

    /**
     * Starts an activity.
     *
     * @param component
     *            Activity ComponentName.
     *            E.g.: com.mycompany.myapp/com.mycompany.myapp.MyActivity
     */
    void startActivity(String component, CallbackContext callbackContext) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setComponent(ComponentName.unflattenFromString(component));
        cordova.getActivity().startActivity(intent);
    }
}
