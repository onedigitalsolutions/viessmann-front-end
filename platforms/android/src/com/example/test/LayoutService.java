package com.example.test;

import com.example.test.CustomLayout;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;
import android.os.*;

public class LayoutService extends Service {

	public static LayoutService instance;
	public static boolean isRunning = false;
	private Runnable destroySelf = new Runnable(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			Intent localIntent = new Intent(LayoutService.this, LayoutService.class);
			LayoutService.this.stopService(localIntent);
			if(LayoutService.this.orientation !=0)
				LayoutService.this.completeKill();
		}
		
	};
	
	private final IBinder mBinder = new LocalBinder();
	int orientation = 0;
	boolean persistent = false;
	int selIndex = 0;
	
	static{
		instance = null;
	}
	
	public static LayoutService getInstance(){
		return instance;
	}
	
	public void completeKill(){
		Process.killProcess(Process.myPid());
	}
	
	public IBinder onBind(Intent paramIntent){
		return this.mBinder;
	}
	
	public void onCreate(){
		
	}
	
	public void onDestroy(){
		isRunning = false;
		instance = null;
		Log.d("androidlock", "destroying service");
	}
	
	public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2){
		instance = this;
		isRunning = true;
		Object  localObject = null;
		this.orientation = paramIntent.getIntExtra("orientation", 0);
		this.persistent = paramIntent.getBooleanExtra("persistent", false);
		this.selIndex = paramIntent.getIntExtra("selIndex", 0);
		CustomLayout localCustomLayout = new CustomLayout(this.orientation);
		return Log.d("androidock", "start service");
		
		
	}
	
	public class LocalBinder extends Binder{
		public LocalBinder(){
			
		}
		
		LayoutService getService(){
			return LayoutService.this;
		}
	}
	

	

}
